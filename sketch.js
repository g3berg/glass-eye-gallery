
// parameters
let p = {

  open: 0,
  openMin: -10,
  openMax: 10,

  consciencious: 0,
  conscienciousMin: -10,
  conscienciousMax: 10,

  extraversion: 0,
  extraversionMin: -10,
  extraversionMax: 10,

  agreeable: 0,
  agreeableMin: -10,
  agreeableMax: 10,


  neurotic: 0,
  neuroticMin: -10,
  neuroticMax: 10,

};

let angle = 0;
let r = 150;
let borderAngle = 0.087;
let palette;
let sections = 6;


function setup() {
  
sections = 3;
if(p.consciencious > 3){
  sections = 6;
}if(p.consciencious < -3){
  sections = 2;
}
if(p.open > 0){
  palette = randomColourGenerator();
}else{
  palette = analogousColourGenerator();
}

let canv = createCanvas(windowWidth-50, windowHeight);
canv.mousePressed(setup);

background(0, 0, 0);
fill(255, 255, 255);
ellipse(width/2, height/2, 1000);
fill(palette[1]);
drawGradient(width/2, height / 2, width/2, palette[1]);
translate(width/2, height/2);
compartments(width/6-30, sections);
palette[1].setAlpha(255);
fill(palette[1]);
spiral();
ellipse(0, 0, 200);
translate(-width/2, -height/2);

for(let i = 0; i < 500; i++){
  let x = random(width);
  let y = random(height);
 if(get(x,y)[0] == round(red(palette[2])) && get(x,y)[1] == round(green(palette[2]) ) && get(x,y)[2] == round(blue(palette[2]))){
  if(abs(x-width/2) > 10 && abs(y - height/2) > 10){
     flower(x,y, "f");
  }
 }


}
for(let i = 0; i < 100; i++){
  let x = random(width);
  let y = random(height);
 if(get(x,y)[0] == round(red(palette[2])) && get(x,y)[1] == round(green(palette[2]) ) && get(x,y)[2] == round(blue(palette[2]))){
  if(abs(x-width/2) > 10 && abs(y - height/2) > 10){
     flower(x,y, "e");
  }
 }


}



translate(width/2, height/2);
fill(0, 0, 0);
noStroke();
drawGradient(0, 0, 300+p.open*2, color(0, 0, 0)); 
translate(-width/2, -height/2);
createParamGui(p, paramChanged);

}

function draw() {

}



// global callback from the settings GUI
function paramChanged(name) {

}

/*
randomColourGenerator:

Returns an array of a colour palette assosiated with high openness. 
It selects three random colours and their compliments, meaning the palette
looks random and high contrast. 

*/

function randomColourGenerator(){

  let arr = [];
 
  r = random(0, 255);
  g = random(0, 255);
  b = random(0, 255);
 
  r2 = random(0, 255);
  g2 = random(0, 255);
  b2 = random(0, 255);
 
 
  r3 = random(0, 255);
  g3 = random(0, 255);
  b3 = random(0, 255);

  arr[0]= color(r, g, b);
  arr[1]= color(255-r, 255-g, 255-b);
  arr[2]=color(r2, g2, b2);
  arr[3]=color(255-r2, 255-g2, 255-b2);
  arr[4]=color(r3, g3, b3);
  arr[5]=color(255-r3, 255-g3, 255-b3);
 
 
  return arr;
 }
 
 /*
analogousColourGenerator:

Returns an array of a colour palette assosiated with low openness. 
It selects one random colour, and chooses a set of colours analogous to the first.
I based the algorithm for this off of the adobe analogous colour picker tool. 

*/
 
 function analogousColourGenerator(){
  let arr = [];
  c = random(0, 3);
   r = random(0, 255);
  g = random(0, 255);
  b = random(0, 255);
 
 
  rr = 10;
  gg = 10;
  bb = 10;
   if(c <= 1){
    rr = 75;
    r = 0;
  }if(c <= 2 && c>1){
    gg = 75;
    g = 0
 
 
  }if(c > 2){
    bb = 75;
    b = 0;
  }
   stroke (0);
 
 
  arr[0]= color(r, g, b);
  arr[1]= color(r+rr, g+gg, b+bb);
  arr[2]= color(r+2*rr, g+2*gg, b+2*bb);
  arr[3]=color(r+3*rr, g+3*gg, b+3*bb);
  arr[4]= color(r+2*gg, g+2*bb, b+2*rr);
  arr[5]=color(r+gg, g+bb, b+rr);
  return arr;
 }
 
 
 /*
drawGradient(x position, y position, l radius, c colour)

Draws an ellipse of radius l. The gradient opacity effect around the 
edges is determined by the degree of agreeableness. This function is used for the colour of the iris
and the pupil. 
 */
 
 function drawGradient(x, y, l, c){ 
  noStroke();
  circleColour = c;
  circleRadius = l;
  let k = map(p.agreeable, p.agreeableMin, p.agreeableMax, 0, 1.7);
  opacityLevel = k; // 0 - 1.7
  initialRadius = (1 - opacityLevel) * circleRadius;
  gradientCircleRadius = (opacityLevel * circleRadius) / 255;
 
 
  ellipse(x, y, initialRadius);
  i  = 0
  r = initialRadius;
 
 
  while(i < 255 && r < circleRadius){
    circleColour.setAlpha(255 - i);
    fill(circleColour);
    i+=1;
    ellipse(x, y, round(r));
    r+=gradientCircleRadius;
  }
 }
 
 
 

 
 /*
compartments(size, num)
Draws "compartments" (the little petal-shaped colours around the pupil)
according to the size/number required. Uses triangles and curves.
Number of compartments depends on conscienciousness. 
 */
 
 function compartments(size, num) {
  fill(palette[1]);
  noStroke();
  compartmentsColour = palette[2];
  fill(compartmentsColour);
  size*=1.3;
 
 
  if(num == 4){
  for (let i = 0; i < 4; i++){
  push();
    rotate(i*PI/2);
    triangle(0, 0, size*cos(borderAngle), size*sin(borderAngle), size*sin(borderAngle), size*cos(borderAngle));
    pop();
  }
  }
  if(num == 3){
      for (let i = 0; i < 3; i++){
  push();
    rotate(i*2*PI/3);
    triangle(0, 0, size*(cos(-1/6*PI+borderAngle)), size*(sin(-1/6*PI+borderAngle)),size*(cos(1/2*PI-borderAngle)), size*(sin(1/2*PI-borderAngle)) );
    beginShape();
    curveVertex(0, 0);
    curveVertex(size*(cos(-1/6*PI+borderAngle)), size*(sin(-1/6*PI+borderAngle)));
    curveVertex(size*(cos(1/6*PI))+random(-5, 5), size*(sin(1/6*PI))+random(-5, 5));
    curveVertex(size*(cos(1/2*PI-borderAngle)), size*(sin(1/2*PI-borderAngle)));
    curveVertex(0, 0);
    endShape();
    pop();
  }
  }
    if(num == 2){
      for (let i = 0; i < 2; i++){
    push();
    rotate(i*PI);
        triangle(0, 0, size*(cos(-1/12*PI+borderAngle)), size*(sin(-1/12*PI+borderAngle)),size*(cos(3/2*PI-borderAngle)), size*(sin(3/2*PI-borderAngle)) );
        beginShape();
        curveVertex(0, 0);
        curveVertex(size*(cos(-1/12*PI+borderAngle)), size*(sin(-1/12*PI+borderAngle)));
        curveVertex(size*(cos(3/2*PI-borderAngle)), size*(sin(3/2*PI-borderAngle)));
        curveVertex(0, 0);
        endShape();
    pop();
  }
  }
    if(num == 6){
      for (let i = 0; i < 6; i++){
    push();
    rotate(i*PI/3);
    triangle(0, 0, size*cos(PI/3 + borderAngle), size*sin(PI/3 + borderAngle), size*sin(PI/3 + borderAngle), size*cos(PI/3 + borderAngle));
    beginShape();
    curveVertex(0, 0);
    curveVertex(size*cos(PI/3 + borderAngle), size*sin(PI/3 + borderAngle));
    curveVertex(size*(cos(3/12*PI))+random(-5, 5), size*(sin(3/12*PI))+random(-5, 5));
    curveVertex(size*sin(PI/3 + borderAngle), size*cos(PI/3 + borderAngle));
    curveVertex(0, 0);
    endShape();
    pop();
  }
  }
 }

  /*
spiral()
Draws a spiral at 0,0. The spiral selected depends on the level of neuroticism.
 */
 
 function spiral(){
  sColour = palette[0];
  let spiralAngle = [25, 50, 100, 200, 10, 77];
  let k = map(p.neurotic, p.neuroticMin, p.neuroticMax, 0, 5);

  let angle = 0;
  let d = 0;
  let px = 0;
  let py = 0;
  for(let i = 0; i < 500; i++){
    sColour.setAlpha(255 - i/2);
    stroke(sColour);
    d += 1;
    angle += spiralAngle[round(k)]; 
    let x = sin(angle) * d
    let y = cos(angle) * d
    line(px, py, x, y)
    px = x;
    py = y;
  }
 }
 
 function flower(m, n, t){
  stroke(255, 255, 255);
  let v2 = createVector(1, 0);
  let v1 =  createVector(m-width/2, n-height/2);
  let ang = v1.angleBetween(v2);
  if(ang < 0){
    ang+=2*PI;
  }
  palette[1].setAlpha(255);
  palette[3].setAlpha(255);
 

  let flowerColour;
 
  let r = red(palette[3]);
  let g = green(palette[3]);
  let bl = blue(palette[3]);
 if(sections == 2){
    if(n > height/2){
      if(random(2) > 1){
        flowerColour = palette[3];
        strokeColour = palette[3];
      }else{
          flowerColour = palette[5];
          strokeColour = palette[5];
      }

    }else{
      if(random(2) > 1){
        flowerColour = palette[4];
        strokeColour = palette[4];
      }else{
          flowerColour = palette[5];
          strokeColour = palette[5];
      }
    }
 }
 if(sections == 3){
  if (m - width / 2 < 0){
    palette[0].setAlpha(255);
    flowerColour = palette[0];
    strokeColour = palette[0];
  }
  if (m - width / 2 > 0){
    flowerColour = palette[4];
    strokeColour = palette[4];
  }
  if (ang > 0.5 && ang <= 2.6){
    flowerColour = palette[3];
    strokeColour = palette[3];
  }
 }
  if(sections == 6){
    flowerColour = palette[1];
    strokeColour = palette[1];
    if (ang > 0.96 && ang <= 1.6){
      flowerColour = palette[5];
      strokeColour = palette[5];
    }
    if (ang > 1.7 && ang <= 3){
      flowerColour = palette[1];
      strokeColour = palette[1];
    }
    if (ang > 3 && ang <= 3.9){
      flowerColour = palette[3];
      strokeColour = palette[3];
    }
    if (ang > 3.9 && ang <= 5){
      flowerColour = palette[5];
      strokeColour = palette[5];

    }
    if (ang > 5 && ang < 6){
      flowerColour = palette[4];
      strokeColour = palette[4];
    }
 
  }
 fill(flowerColour);
 stroke(strokeColour);
 let radius = random(50+p.extraversion);
 let a = random(30+p.extraversion);
 let b = random(30+p.extraversion);

 if(t =="o"){
  fill(0,0,0,0);
 }
 
 
 if(t == "f" || t =="o"){
 beginShape();
 for(let i = 0; i <= 2*PI; i+=0.06) {
  let x = (radius + a * sin(b*i)) * cos(i) + m;
  let y = (radius + a * sin(b*i)) * sin(i) + n;
  vertex(x, y);
 }
 vertex((radius + a * sin(0)) * cos(0) + m, (radius + a * sin(0)) * sin(0) + n);
 endShape();
 }
 if(t == "e"){
  ellipse(m, n, 10);
 }
 }
 
